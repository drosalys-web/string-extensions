<?php

/*
 * This file is part of the drosalys-web/string-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\StringExtensions\Random;

use Ramsey\Uuid\Uuid;

/**
 * Class TokenGenerator
 *
 * @author Benjamin Georgeault
 */
class TokenGenerator implements TokenGeneratorInterface
{
    /**
     * @inheritDoc
     */
    public function generateToken(): string
    {
        return rtrim(strtr(base64_encode(Uuid::uuid4()->getHex()), '+/', '-_'), '=');
    }
}
