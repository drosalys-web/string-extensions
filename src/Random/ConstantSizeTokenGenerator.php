<?php

/*
 * This file is part of the drosalys-web/string-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\StringExtensions\Random;

/**
 * Class ConstantSizeTokenGenerator
 *
 * @author Benjamin Georgeault
 */
class ConstantSizeTokenGenerator implements TokenGeneratorInterface
{
    /**
     * @var int
     */
    private $size;

    /**
     * @var TokenGenerator
     */
    private $generator;

    /**
     * ConstantSizeTokenGenerator constructor.
     * @param int $size
     */
    public function __construct(int $size = 12)
    {
        $this->size = $size;
        $this->generator = new TokenGenerator();
    }

    /**
     * @inheritDoc
     */
    public function generateToken(): string
    {
        $token = '';
        $i = 0;
        do {
            $token .= (string) $this->generator->generateToken();
            $token = substr($token, 0, $this->size);

            if (20 < $i++) {
                throw new \RuntimeException('Too many rotation for token generation.');
            }
        } while ($this->size > strlen($token));

        return $token;
    }
}
