<?php

/*
 * This file is part of the drosalys-web/string-extensions package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys-web.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\StringExtensions\Bridge\Doctrine\ORM\Id;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;
use DrosalysWeb\StringExtensions\Random\ConstantSizeTokenGenerator;

/**
 * Class ConstantSizeIdGenerator
 *
 * @author Benjamin Georgeault
 */
class ConstantSizeIdGenerator extends AbstractIdGenerator
{
    /**
     * @var ConstantSizeTokenGenerator
     */
    private $tokenGenerator;

    public function __construct()
    {
        $this->tokenGenerator = new ConstantSizeTokenGenerator();
    }

    /**
     * @param EntityManager $em
     * @param null|object $entity
     * @return string
     * @throws \Exception
     */
    public function generate(EntityManager $em, $entity)
    {
        $id = $this->tokenGenerator->generateToken();

        if (null !== $entity && null !== $em->getRepository(get_class($entity))->findOneBy(['id' => $id])) {
            $id = $this->generate($em, $entity);
        }

        return $id;
    }
}
